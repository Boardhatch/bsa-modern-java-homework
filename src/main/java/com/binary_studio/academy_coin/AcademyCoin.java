package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		int profit = 0;
		Integer[] array = prices.toArray(Integer[]::new);
		boolean incremented = false;
		int i = 0;
		while (i < array.length - 1) {
			if (array[i] < array[i + 1]) {
				int k = (i + 1);
				while (k < array.length - 1) {
					if (array[k] > array[k + 1]) {
						profit += array[k] - array[i];
						incremented = true;
						i = k;
						break;
					}
					k++;
				}
				if (!incremented) {
					profit += array[k] - array[i];
				}
				i = k;
				incremented = false;
			}
			i++;
		}
		return profit;
	}

}
