package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class DependencyDetector {

	private DependencyDetector() {
	}
	private static boolean added = false;

	public static boolean canBuild(DependencyList libraries) {
		List<Set<String>> cycles = new ArrayList<>();
		for (String[] dependency : libraries.dependencies) {
			if (cycles.isEmpty()) {
				cycles.add(createNewNode(dependency));
			}
			else {
				if (isCreatingLoop(cycles, dependency)) {
					return false;
				}
				if (!added) {
					cycles.add(createNewNode(dependency));
				}
				added = false;
			}
		}
		return true;
	}


	private static boolean isCreatingLoop(List<Set<String>> cycles, String[] dependency) {
		for (Set<String> s : cycles) {
			if (s.contains(dependency[0])) {
				if (!s.add(dependency[1])) {
					return true;
				}
				added = true;
				return false;
			}
		}
		return false;
	}

	private static Set<String> createNewNode(String[] dependency) {
		Set<String> hashSet = new HashSet<>();
		hashSet.add(dependency[0]);
		hashSet.add(dependency[1]);
		return hashSet;
	}

}
