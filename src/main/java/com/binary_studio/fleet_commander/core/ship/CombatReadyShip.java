package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private final DockedShip combatShip;

	public CombatReadyShip(DockedShip dockedShip) {
		this.combatShip = dockedShip;
	}

	@Override
	public void endTurn() {
		this.combatShip.restoreCapacity();
	}

	@Override
	public void startTurn() {
		// nothing
	}

	@Override
	public String getName() {
		return this.combatShip.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.combatShip.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.combatShip.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.combatShip.executeSubsystem(this.combatShip.getAttackSubsystem())) {
			PositiveInteger damage = this.combatShip.getAttackSubsystem().attack(target);
			return Optional.of(new AttackAction(damage, this, target, this.combatShip.getAttackSubsystem()));
		}
		return Optional.empty();
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		PositiveInteger actualDamage = PositiveInteger
				.of(this.combatShip.getDefenciveSubsystem().reduceDamage(attack).damage.value());
		if (this.combatShip.canHandleDamage(actualDamage)) {
			this.combatShip.applyDamage(actualDamage);
			return new AttackResult.DamageRecived(attack.weapon, actualDamage, attack.target);
		}
		else {
			this.combatShip.applyDamage(actualDamage);
			return new AttackResult.Destroyed();
		}

	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.combatShip.executeSubsystem(this.combatShip.getDefenciveSubsystem())) {
			return Optional.of(new RegenerateAction(this.combatShip.restoreShield(), this.combatShip.restoreHull()));
		}
		else {
			return Optional.empty();
		}
	}

}
