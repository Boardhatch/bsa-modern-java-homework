package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger capacitor;

	private final PositiveInteger capacitorRegeneration;

	private final PositiveInteger pg;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private PositiveInteger pgRemind;

	private PositiveInteger capacityRemind;

	private PositiveInteger hullRemind;

	private PositiveInteger shieldRemind;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
		this.pgRemind = pg;
		this.capacityRemind = capacitor;
		this.hullRemind = hullHP;
		this.shieldRemind = shieldHP;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		if (name == null || name.isBlank()) {
			throw new IllegalStateException("Name should be not null and not empty");
		}
		return new DockedShip(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, powergridOutput, speed,
				size);
	}

	public boolean canHandleDamage(PositiveInteger damage) {
		return (this.hullRemind.value() + this.shieldRemind.value()) - damage.value() > 0;
	}

	public void applyDamage(PositiveInteger damage) {
		if (canHandleDamage(damage)) {
			int damageRemind = damage.value();
			if (this.shieldRemind.value() - damageRemind < 0) {
				damageRemind -= this.shieldRemind.value();
				this.hullRemind = PositiveInteger.of(this.hullRemind.value() - damageRemind);
				this.shieldRemind = PositiveInteger.of(0);
			}
			else {
				this.shieldRemind = PositiveInteger.of(this.shieldRemind.value() - damageRemind);
			}
		}
		else {
			this.shieldRemind = PositiveInteger.of(0);
			this.hullRemind = PositiveInteger.of(0);
		}
	}

	public boolean executeSubsystem(Subsystem subsystem) {
		if (this.capacityRemind.value() >= subsystem.getCapacitorConsumption().value()) {
			this.capacityRemind = PositiveInteger
					.of(this.capacityRemind.value() - subsystem.getCapacitorConsumption().value());
			return true;
		}
		return false;
	}

	public void restoreCapacity() {
		PositiveInteger restoredCapacity;
		if (this.capacityRemind.value() + this.capacitorRegeneration.value() > this.capacitor.value()) {
			restoredCapacity = PositiveInteger.of(this.capacitor.value() - this.capacityRemind.value());
		}
		else {
			restoredCapacity = PositiveInteger.of(this.capacitorRegeneration.value());
		}
		this.capacityRemind = PositiveInteger.of(this.capacityRemind.value() + restoredCapacity.value());
	}

	public PositiveInteger restoreShield() {
		PositiveInteger restored;
		RegenerateAction regen = this.defenciveSubsystem.regenerate();
		if (this.shieldRemind.value() + regen.shieldHPRegenerated.value() > this.shieldHP.value()) {
			restored = PositiveInteger.of(this.shieldHP.value() - this.shieldRemind.value());
		}
		else {
			restored = PositiveInteger.of(regen.shieldHPRegenerated.value());
		}
		this.shieldRemind = PositiveInteger.of(this.shieldRemind.value() + restored.value());
		return restored;
	}

	public PositiveInteger restoreHull() {
		PositiveInteger restored;
		RegenerateAction regen = this.defenciveSubsystem.regenerate();
		if (this.hullRemind.value() + regen.hullHPRegenerated.value() > this.hullHP.value()) {
			restored = PositiveInteger.of(this.hullHP.value() - this.hullRemind.value());
		}
		else {
			restored = PositiveInteger.of(regen.hullHPRegenerated.value());
		}
		this.hullRemind = PositiveInteger.of(this.hullRemind.value() + restored.value());
		return restored;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			if (this.attackSubsystem != null) {
				this.pgRemind = PositiveInteger
						.of(this.pgRemind.value() + this.attackSubsystem.getPowerGridConsumption().value());
			}
			this.attackSubsystem = null;
		}
		else {
			checkPg(subsystem);
			this.attackSubsystem = subsystem;
			this.pgRemind = PositiveInteger.of(this.pgRemind.value() - subsystem.getPowerGridConsumption().value());
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			if (this.defenciveSubsystem != null) {
				this.pgRemind = PositiveInteger
						.of(this.pgRemind.value() + this.defenciveSubsystem.getPowerGridConsumption().value());
			}
			this.defenciveSubsystem = null;
		}
		else {
			checkPg(subsystem);
			this.defenciveSubsystem = subsystem;
			this.pgRemind = PositiveInteger.of(this.pgRemind.value() - subsystem.getPowerGridConsumption().value());
		}
	}

	private void checkPg(Subsystem subsystem) throws InsufficientPowergridException {
		if (subsystem.getPowerGridConsumption().value() > this.pgRemind.value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - this.pgRemind.value());
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else {
			return new CombatReadyShip(this);
		}
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

}
