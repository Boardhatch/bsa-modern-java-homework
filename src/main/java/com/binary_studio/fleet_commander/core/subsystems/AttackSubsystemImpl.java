package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger baseDamage;

	private final PositiveInteger optimalSize;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger capacitorUsage;

	private final PositiveInteger pgRequirement;

	private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
		this.capacitorUsage = capacitorConsumption;
		this.pgRequirement = powergridRequirments;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1
				: (1.0 * target.getSize().value()) / this.optimalSize.value();
		double speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
				: this.optimalSpeed.value() / (2.0 * target.getCurrentSpeed().value());
		return PositiveInteger
				.of((int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
